<?xml version="1.0" encoding="UTF-8"?>


<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    
    xmlns:xs="http://www.w3.org/2001/XMLSchema"
    exclude-result-prefixes="xs"
    version="2.0">
    
    <xsl:output
        method="html"
        doctype-system="about:legacy-compat"
        encoding="UTF-8"
        indent="yes" />
    
    <xsl:template match="/">
        <html>
            <head>
                <link href="cdm_rugby.css" type="text/css" rel="stylesheet"/>
                <title> Coupe du monde de rugby 2019 </title>
            </head>
            <body>
                <title>Coupe du monde de rugby 2019</title>
                <p>
                    <h1>Japan Rugby World Cup 2019</h1>
                    <p>
                        <l>Cette page contient tous les résultats de la coupe du monde de rugby 2019 qui a eu lieu au Japon. Elle contient aussi les informations sur les stades qui ont accueilli les matchs de cette coupe du monde.</l>
                    </p>
                </p>
                <xsl:apply-templates select="//poules"/>
                <h2>Récapitulatif des matches de poules :</h2>
                <xsl:call-template name="matches">
                    <xsl:with-param name="type">premier_tour</xsl:with-param>
                </xsl:call-template>              
                <h2>Quart de finale :</h2>
                <xsl:call-template name="matches">
                    <xsl:with-param name="type">quarts_de_finale</xsl:with-param>
                </xsl:call-template>
                <h2>Demi finales :</h2>
                <xsl:call-template name="matches">
                    <xsl:with-param name="type">demi_finales</xsl:with-param>
                </xsl:call-template>
                <h2>Petite finale :</h2>
                <xsl:call-template name="matches">
                    <xsl:with-param name="type">petite_finale</xsl:with-param>
                </xsl:call-template>
                <h2>Finale :</h2>
                <xsl:call-template name="matches">
                    <xsl:with-param name="type">finale</xsl:with-param>
                </xsl:call-template>
                <h2>Annexe : Villes accueillant la coupe du monde</h2>
                <xsl:apply-templates select="//lieux"/>
            </body>
        </html>
    </xsl:template>
    
    <xsl:template match="//poules">
        <xsl:for-each select="poule">
            <h2 class="poule">Poule : <xsl:value-of select="@id"/></h2>
            <table>
                <tr>
                    <th>Pays</th>
                    <th>Victoires</th>
                    <th>Matchs nuls</th>
                    <th>Défaites</th>
                    <th>Bonus Offensif</th>
                    <th>Bonus Défensif</th>
                    <th>Score</th>
                </tr>
                <xsl:for-each select="equipe_poule">
                    <xsl:variable name="code" select="@code"/>
                    <tr>
                        <td><xsl:value-of select="//equipes/equipe[@code=$code]/pays"/></td>
                        <td><xsl:value-of select="@nb_victoires"/></td>
                        <td><xsl:value-of select="@nb_nuls"/></td>
                        <td><xsl:value-of select="@nb_defaites"/></td>
                        <td><xsl:value-of select="@nb_bonus_off"/></td>
                        <td><xsl:value-of select="@nb_bonus_def"/></td>
                        <td><xsl:value-of select="points"/></td>
                    </tr>
                </xsl:for-each>
            </table>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template name="matches">
        <xsl:param name="type"/>
        <xsl:param name="poule"/>
        <xsl:for-each select="//*/match">
            <xsl:if test="$type = name(..) or $type = name(../..)">
                <p></p>
                <table>
                    <tr>
                        <xsl:if test="$type = 'premier_tour'">
                            <th>Poule</th>
                        </xsl:if>
                        <th>Date</th>
                        <th>Stade</th>
                        <th>Equipe 1</th>
                        <th>Score</th>
                        <th>Equipe 2</th>
                    </tr>
                    <tr>
                        <xsl:variable name="code1" select="equipe_match[position() = 1]/@code"/>
                        <xsl:variable name="code2" select="equipe_match[position() = 2]/@code"/>
                        <xsl:variable name="score1" select="equipe_match[position() = 1]/score"/>
                        <xsl:variable name="score2" select="equipe_match[position() = 2]/score"/>
                        <xsl:variable name="lieu" select="@lieu"/>
                        <xsl:if test="$type = 'premier_tour' ">
                            <td><xsl:value-of select="@poule"/></td>
                        </xsl:if>
                        
                        <td><xsl:value-of select="@date"/></td>
                        <td><a><xsl:attribute name="href">#<xsl:value-of select="@lieu"/></xsl:attribute><xsl:value-of select="//lieux/lieu[@id=$lieu]/stade"/></a></td>
                        <xsl:choose>
                            <xsl:when test="$score1 &gt; $score2">
                                <td><mark><xsl:value-of select="//equipes/equipe[@code=$code1]/pays"/></mark></td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td><xsl:value-of select="//equipes/equipe[@code=$code1]/pays"/></td> 
                            </xsl:otherwise>
                        </xsl:choose>
                        <td><xsl:value-of select="$score1"/> - <xsl:value-of select="$score2"/></td>
                        <xsl:choose>
                            <xsl:when test="$score2 &gt; $score1">
                                <td><mark><xsl:value-of select="//equipes/equipe[@code=$code2]/pays"/></mark></td>
                            </xsl:when>
                            <xsl:otherwise>
                                <td><xsl:value-of select="//equipes/equipe[@code=$code2]/pays"/></td> 
                            </xsl:otherwise>
                        </xsl:choose>
                        
                    </tr>
                </table>
            </xsl:if>
        </xsl:for-each>
    </xsl:template>
    
    <xsl:template match="//lieux">
        
        <ul>
            <xsl:for-each select="lieu">
                
                <li><xsl:attribute name="id"><xsl:value-of select="@id"/></xsl:attribute><xsl:value-of select="ville"/> - Stade : <xsl:value-of select="stade"/> (capacité : <xsl:value-of select="stade/@capacite"/> places)</li>
                           
            </xsl:for-each>
        </ul>
        
    </xsl:template>
    
    
    
</xsl:stylesheet>